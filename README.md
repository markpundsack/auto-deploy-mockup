## Mock up of auto deploy project

* `.gitlab-ci.yml` contains standard pipeline definition for user to modify as needed
* `.gitlab` directory contains copies of standard "plugins"
* `.gitlab/auto_deploy` contains standard pipeline job definitions; depends on container scheduler plugin to also be installed and included first
* `.gitlab/kubernetes-auto-deploy` contains bash scripts for auto deploying using Kubernetes, and a small YAML file to include in CI/CD configuration

See https://gitlab.com/gitlab-org/gitlab-ce/issues/29412 for discussion.
